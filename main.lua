-- Cette ligne permet d'afficher des traces dans la console pendant l'éxécution
io.stdout:setvbuf('no')

-- Cette ligne permet de déboguer pas à pas dans ZeroBraneStudio
if arg[#arg] == "-debug" then require("mobdebug").start() end

function math.angle(x1,y1, x2,y2) return math.atan2(y2-y1, x2-x1) end

FOLDER_FONTS = "assets/fonts"
FOLDER_IMAGES = "assets/images"
FOLDER_SOUNDS = "assets/sounds"
FOLDER_MAP = "assets/map"
FOLDER_MAP_IMAGES = FOLDER_MAP.."/images"
FOLDER_MAP_LEVELS = FOLDER_MAP.."/levels"

SPRITE_TYPE_UNDEFINED = "sprite_type_undefined"
SPRITE_TYPE_PLAYER = "sprite_type_player"

SPRITE_STATE_NORMAL = "sprite_state_normal"
SPRITE_STATE_SLEEPING = "sprite_state_sleeping"
SPRITE_STATE_DESTROYED = "sprite_state_destroyed"
SPRITE_STATE_DESTROYING = "sprite_state_destroying"

SPRITE_TYPE_PNJ = "sprite_type_pnj" -- un PNJ
SPRITE_TYPE_PNJ__1 = "sprite_type_pnj__1" -- vaisseau 1
SPRITE_TYPE_PNJ__2 = "sprite_type_pnj__2" -- vaisseau 2
SPRITE_TYPE_PNJ__3 = "sprite_type_pnj__3" -- vaisseau 3
SPRITE_TYPE_PNJ__4 = "sprite_type_pnj__4" -- tourelle
SPRITE_TYPE_PNJ__BOSS = "sprite_type_pnj__boss" -- un PNJ boss
SPRITE_TYPE_PNJ__BOSS__1 = "sprite_type_pnj__boss__1" -- boss 1

SPRITE_TYPE_PROJ_PLAYER= "sprite_type_proj_player"
SPRITE_TYPE_PROJ_PLAYER__1 = "sprite_type_proj_player__1"
SPRITE_TYPE_PROJ_PLAYER__2 = "sprite_type_proj_player__2"
SPRITE_TYPE_PROJ_PLAYER__3 = "sprite_type_proj_player__3"
SPRITE_TYPE_PROJ_PNJ = "sprite_type_proj_pnj"
SPRITE_TYPE_PROJ_PNJ__1 = "sprite_type_proj_pnj__1"
SPRITE_TYPE_PROJ_PNJ__2 = "sprite_type_proj_pnj__2"
SPRITE_TYPE_PROJ_PNJ__3 = "sprite_type_proj_pnj__3"

SPRITE_TYPE_VFX = "sprite_type_vfx"
SPRITE_TYPE_VFX__EXPLODE = "sprite_type_vfx__explode"
SPRITE_TYPE_VFX__EXPLODE_PLAYER = "sprite_type_vfx__explode_player"
SPRITE_TYPE_VFX__EXPLODE_PNJ = "sprite_type_vfx__explode_pnj"
SPRITE_TYPE_VFX__EXPLODE_BOSS = "sprite_type_vfx__explode_boss"

SCREEN_START = "screen_start"
SCREEN_PLAY = "screen_play"
SCREEN_END = "screen_end"

-- Empêche Love de filtrer les contours des images quand elles sont redimentionnées
-- Indispensable pour du pixel art
love.graphics.setDefaultFilter("nearest")

-- échelle de dessin par défaut pour les sprites
local spriteScale = 2
-- multiplication des vitesses lors de l'application du delta time (évite de modifier toutes les vitesses)
local coeffForSpeedWithDt = 60
-- multiplication des vitesses de tirs lors de l'application du delta time (évite de modifier toutes les vitesses)
local coeffForFireRateWithDt = 30
-- polices d'affichage
local fontDebug
local fontNormal
local fontMenu
local fontTitle
-- message à afficher au centre de l'écran 
local screenMessage = {
  -- texte du message, s'il est vide, rien ne sera affiché, sinon un message sera affiché pendant le timerDelai s 
  text = "",
  -- durée en s d'affichage du message  par défaut 
  delai = 5, 
  -- délai courant d'affichage du message 
  timer = 0, 
  -- police d'affichage
  font = fontNormal,
  -- couleur
  color = {255, 128, 0, 255}
}

-- liste de sprites
local listSprites = {}
local listProjectiles = {}
local listPNJ = {}
-- map
local map = nil
-- camera
local camera = {}
-- joueur (sprites)
local player = nil
-- écran courant
local screen = SCREEN_START
local startScreenImage 
local endScreenImageLose 
local endScreenImageWin
-- images des explosions
local imgExplosionPlayer = {}
local imgExplosionPNJ = {}
-- niveau à atteindre pour gagner
local levelToWin = 3 -- TODO: créer de nouvelle map et augmenter cette valeur, ou alors la calculer automatiquement en fonction du nombre de level existant
-- score du joueur
local playerScore = 0
-- niveau du joueur
local playerLevel = 1
-- le joueur a t'il gagné ?
local playerHasWon = false

function love.load ()
  math.randomseed(love.timer.getTime())
  love.window.setMode(1024,768)
  love.window.setTitle("GCG Shooter")
  screenWidth = love.graphics.getWidth()
  screenHeight = love.graphics.getHeight()

  fontDebug = love.graphics.newFont(FOLDER_FONTS.."/SPACEMAN.TTF", 10)
  fontNormal = love.graphics.newFont(FOLDER_FONTS.."/SPACEMAN.TTF", 18)
  fontMenu = love.graphics.newFont(FOLDER_FONTS.."/SPACEMAN.TTF", 25)
  fontTitle = love.graphics.newFont(FOLDER_FONTS.."/SPACEMAN.TTF", 35)

  love.graphics.setFont(fontNormal)
  
  startScreenImage = love.graphics.newImage (FOLDER_IMAGES.."/startScreenImage.jpg")
  endScreenImageLose = love.graphics.newImage (FOLDER_IMAGES.."/endScreenImageLose.jpg")
  endScreenImageWin = love.graphics.newImage (FOLDER_IMAGES.."/endScreenImageWin.jpg")

  -- charge les images pour l'animation des explosions
  -- TODO: mettre des images différentes pour les PNJ et le player

  local index
  for index = 1, 5 do
    imgExplosionPlayer[index] = love.graphics.newImage (FOLDER_IMAGES.."/explode_"..index..".png")
    imgExplosionPNJ[index] = love.graphics.newImage (FOLDER_IMAGES.."/explode_"..index..".png")
  end

  -- TODO: mettre des sons différents pour les PNJ et le player
  sndPlayerFire = love.audio.newSource(FOLDER_SOUNDS.."/shoot.wav", static)
  sndpnjFire = love.audio.newSource(FOLDER_SOUNDS.."/shoot.wav", static)
  sndPlayerLoseLife = love.audio.newSource(FOLDER_SOUNDS.."/explode_touch.wav", static)
  sndpnjLoseLife = love.audio.newSource(FOLDER_SOUNDS.."/explode_touch.wav", static)
  sndPlayerDestroy = love.audio.newSource(FOLDER_SOUNDS.."/explode_touch.wav", static)
  sndpnjDestroy = love.audio.newSource(FOLDER_SOUNDS.."/explode_touch.wav", static)
  
  screenMessage.font = fontTitle
  screenMessage.color = {255, 128, 0, 255}
end --love.load

function love.update (dt)
  -- UPDATE INDéPENDANTS DE L'éCRAN
  -----------------------------------
  HUDUpdate(dt)
  
  -- UPDATE DéPENDANTS DE L'éCRAN
  -----------------------------------
  if (screen == SCREEN_PLAY) then
    updatePlayScreen(dt)
  elseif (screen == SCREEN_START) then
    updateStartScreen(dt)
  elseif (screen == SCREEN_END) then
    updateEndScreen(dt)
  end
end

function love.draw ()
  -- DRAW INDéPENDANTS DE L'éCRAN
  -----------------------------------
  HUDDraw()

  -- DRAW DéPENDANTS DE L'éCRAN
  -----------------------------------
  if (screen == SCREEN_PLAY) then
    drawPlayScreen()
  elseif (screen == SCREEN_START) then
    drawStartScreen()
  elseif (screen == SCREEN_END) then
    drawEndScreen()
  end
end

function love.keypressed (key)
  -- CLAVIER : gestion des touche globales à l'application et discontinues pour le joueur

  -- TOUCHES INDéPENDANTES DE L'éCRAN
  -----------------------------------
  if (key == "escape") then love.event.push("quit") end


  -- TOUCHES INDéPENDANTES DE L'éCRAN
  -----------------------------------
  if (screen == SCREEN_PLAY) then
    keypressedPlayScreen(key)
  elseif (screen == SCREEN_START) then
    keypressedStartScreen(key)
  elseif (screen == SCREEN_END) then
    keypressedEndScreen(key)
  end
end

function  updatePlayScreen (dt)
  -- TODO: utiliser la valeur dt pour les calculs de vélocité (attention toutes les vitesses doivent au moins être multipliée par 10)
  local index, index2, projectile
  local PNJ
  local xMax, yMax, xMin, yMin
  
  local speedCoeff = coeffForSpeedWithDt * dt

  -- CAMERA : déplacements en fonction de sa vitesse
  camera.x = camera.x + camera.vX * speedCoeff
  camera.y = camera.y + camera.vY * speedCoeff

  -- JOUEUR: déplacements avec le clavier
  ---------------------------
  -- limites de l'écran pour le déplacement du sprite (tient compte des dimensions du sprite)
  xMin, yMin, xMax, yMax= getScreenLimits(player)

  if (love.keyboard.isDown("right")  and player.x < xMax) then
    player.x = player.x + player.vX * speedCoeff
  elseif (love.keyboard.isDown("left")  and player.x > xMin) then
    player.x = player.x - player.vX * speedCoeff
  end
  if (love.keyboard.isDown("up") and player.y > yMin) then
    player.y = player.y - player.vY * speedCoeff
  elseif (love.keyboard.isDown("down") and player.y < yMax) then
    player.y = player.y + player.vY * speedCoeff
  end

  -- PROJECTILES: déplacements et collisions avec les bords de l'écran en fonction de leur vitesse
  ---------------------------
  for index = #listProjectiles, 1, -1 do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
    projectile = listProjectiles[index]
    xMin, yMin, xMax, yMax= getScreenLimits(projectile)

    -- déplace le projectile avec ses vitesses propres en X et Y
    projectile.x = projectile.x + projectile.vX * speedCoeff
    projectile.y = projectile.y + projectile.vY * speedCoeff

    if (projectile.type:find(SPRITE_TYPE_PROJ_PLAYER) ~= nil) then
      -- projectile du joueur
      
      -- touche t-il un PNJ ?
      -- boucle sur les PNJ
      for index2 = #listPNJ, 1, -1 do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
        PNJ = listPNJ[index2]
        if (spriteBoxCollide(projectile, PNJ)) then
          -- oui
          -- le projectile perd une vie
          projectileLoseLife(projectile, index)
          -- le PNJ perd une vie
          pnjLoseLife(PNJ, index2)
        end
      end
    elseif (projectile.type:find(SPRITE_TYPE_PROJ_PNJ) ~= nil) then
      -- projectile d'un PNJ 
      
      -- touche t-il le joueur ?
      if (projectile.status ~= SPRITE_STATE_DESTROYING and projectile.status ~= SPRITE_STATE_DESTROYED and spriteBoxCollide(projectile, player)) then
        -- oui
        -- le projectile perd une vie
        projectileLoseLife(projectile, index)
        -- le joueur perd une vie
        playerLoseLife()
      end
    end
  
    -- vérifie si le projectile est sorti de l'écran et qu'il n'a pas déjà été supprimé lors d'une collision
    if ((projectile.x < xMin or projectile.x > xMax or projectile.y < yMin or projectile.y > yMax) and projectile. status ~= SPRITE_STATE_DESTROYED) then
      -- oui, on le supprime
      projectileDestroy(projectile, index, false)
    end
  end --for index = #listProjectiles, 1, -1 do

  -- PNJ (ennemis): déplacements et collisions avec les bords de l'écran en fonction de leur vitesse
  ---------------------------
  for index = #listPNJ, 1, -1 do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
    PNJ = listPNJ[index]
    xMin, yMin, xMax, yMax= getScreenLimits(PNJ)

    -- déplace le PNJ avec la vitesse de la caméra
    PNJ.x = PNJ.x + camera.vX * speedCoeff
    PNJ.y = PNJ.y + camera.vY * speedCoeff

    if (PNJ.status == SPRITE_STATE_SLEEPING) then
      -- si le PNJ est endormi, il ne se déplace pas

      -- s'il devient visible dans l'écran, on le réveille
      if (PNJ.x > xMin)  and (PNJ.x < xMax) and (PNJ.y > yMin) and (PNJ.y < yMax) then
        PNJ.status = SPRITE_STATE_NORMAL
      end
    else
      -- si le PNJ n'est pas endormi, il se déplace avec sa vitesse propre (en plus de celle de la camera)

      if (PNJ.status ~= SPRITE_STATE_DESTROYING and PNJ.status ~= SPRITE_STATE_DESTROYED) then
        -- on décrémente le timer de tir du PNJ
        PNJ.fireTimer = PNJ.fireTimer - 1
        if (PNJ.fireTimer <= 0) then
          -- si le timer est écoule , on réinitialise avec un valeur du firerate et le PNJ tire un projectile
          PNJ.fireTimer = math.random(PNJ.fireRate.min, PNJ.fireRate.max)
          pnjFire(PNJ)
        end
      end
      
      -- déplace le PNJ avec ses vitesses propres en X et Y
      PNJ.x = PNJ.x + PNJ.vX * speedCoeff
      PNJ.y = PNJ.y + PNJ.vY * speedCoeff

      -- vérifie si le PNJ a touché le joueur
      if (PNJ.status ~= SPRITE_STATE_DESTROYING and PNJ.status ~= SPRITE_STATE_DESTROYED and spriteBoxCollide(PNJ, player)) then
        -- oui
        -- le joueur perd une vie
        playerLoseLife()
        -- le PNJ perd une vie
        pnjLoseLife(PNJ, index)
      end
      
      -- vérifie si le PNJ est sorti de l'écran en xMin ou xMax
      if (PNJ.x < xMin) then
        -- oui, on la vitesse en X devient positive
        PNJ.vX = math.abs(PNJ.vX)
        PNJ.x = xMin
      elseif (PNJ.x > xMax) then
        -- oui, on la vitesse en X devient négative
        PNJ.vX = -math.abs(PNJ.vX)
        PNJ.x = xMax
      end
      -- vérifie si le PNJ est sorti de l'écran en yMin
      if (PNJ.y < yMin) then
        -- oui, on la vitesse en Y devient positive
        PNJ.vY = math.abs(PNJ.vY) -- vitesse positive
        PNJ.y = yMin
      end
    end --if (PNJ.status == SPRITE_STATE_SLEEPING)

    -- vérifie si le PNJ est sorti de l'écran en yMax (même s'il est en status sleeping)
    if(PNJ.y > yMax) then
      -- doit il être détruit s'il touche le bas ?
      if (PNJ.destroyAtBottom) then 
        -- oui, on le détruit
        pnjDestroy(PNJ, index, false) 
      else
        -- non, on la vitesse en Y devient négative
        PNJ.vY = -math.abs(PNJ.vY) -- vitesse négative
        PNJ.y = yMax
      end
    end

  end --for index = #listPNJ, 1, -1 do

  -- boucle de traitement et de suppression des sprites marqués comme détruits (SPRITE_STATE_DESTROYED) ou à détruire (SPRITE_STATE_DESTROYING)
  ---------------------------
  local sprite
  for index =#listSprites, 1, -1  do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
    sprite = listSprites[index]
    
    if (sprite.maxFrame > 1) then
      -- animation de l'explosion du sprite
      sprite.frame = sprite.frame + speedCoeff / 5
      if (sprite.frame > sprite.maxFrame) then
        -- l'animation du sprite est terminée, on détruit l'explosion
        sprite.status = SPRITE_STATE_DESTROYED
        -- s'il s'agit de l'explosion du joueur, on termine la partie
        if (sprite.type == SPRITE_TYPE_VFX__EXPLODE_PLAYER) then loseGame() end
      else
        -- on affiche la frame suivante (de l'explosion)
        sprite.image = sprite.animationFrames[math.floor(sprite.frame)]
      end
    end
    
    -- le sprite est-il en cours de destruction ? 
    if (sprite.status == SPRITE_STATE_DESTROYING) then 
        -- le délai de destruction est il écoulé ?
      if (sprite.destroyDelay <= 0 ) then 
        -- oui, on le marque à détruire
        sprite.status = SPRITE_STATE_DESTROYED
        if (sprite.type == SPRITE_TYPE_PLAYER) then 
          -- si c'est le joueur, la partie est perdue
          loseGame() 
        elseif (sprite.type:find(SPRITE_TYPE_PNJ__BOSS) ~= nil) then 
          -- si c'est un boss, on passe au niveau suivant
          nextLevel()
        end
      else
        sprite.destroyDelay  = sprite.destroyDelay - dt
      end
    end
    
    -- le sprite doit-il être supprimé ? 
    if (sprite.status == SPRITE_STATE_DESTROYED) then 
      table.remove(listSprites, index) 
    end
  end
  
end --love.update


function updateStartScreen (dt)
  --PLACEHOLDER: rien à faire pour le moment
end --updateStartScreen

function updateEndScreen (dt)
  --PLACEHOLDER: rien à faire pour le moment
end --updateEndScreen

function drawPlayScreen ()
  -- AFFICHAGE de la MAP
  mapDraw()

  -- SPRITES: affichage de tous les sprites sans distinctions
  ---------------------------
  local index, sprite, scale, w ,h
  for index =#listSprites, 1, -1  do -- le parcours décroissant permet de gérer un table.remove dans une liste en cours de parcours
    sprite = listSprites[index]
    drawSprite(sprite)   
  end
  
  love.graphics.setColor(167, 79, 255, 255)
  love.graphics.setFont(fontNormal)
  content = "Score "..playerScore
  w = fontNormal:getWidth(content)
  love.graphics.print(content, 5, 5)
  
  content = "Niveau "..playerLevel
  w = fontNormal:getWidth(content)
  love.graphics.print(content, (screenWidth - w) / 2, 5)
  
  content = "Vies "..player.life
  w = fontNormal:getWidth(content)
  love.graphics.print(content, (screenWidth - w - 5), 5)
  
  love.graphics.setColor(255, 255, 255, 255)

  if (false) then -- DEBUG only
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(fontDebug)
    content = "Projectiles:"..#listProjectiles.." Sprites:"..#listSprites.." pnj:"..#listPNJ
    h = fontDebug:getHeight(content)
    love.graphics.print(content, 0, screenHeight - h)
  end
end --drawPlayScreen

function drawStartScreen ()
  love.graphics.draw (startScreenImage)
  local w, h, font, x, y
  font = fontMenu
  
  love.graphics.setFont(font)
  love.graphics.setColor(255, 255, 255, 255)
  h = font:getHeight(content)
  y = (screenHeight - h) / 2
  
  content = "Detruisez tous vos ennemis et restez en vie"
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + h * 6
  love.graphics.print(content, x , y) 

  content = "Atteignez le niveau "..levelToWin.." pour gagner"
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + h * 1
  love.graphics.print(content, x , y) 
  
  content = "Utilisez les FLECHES pour vous deplacer"
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + h * 2
  love.graphics.print(content, x , y) 

  content = "Appuyer sur ESPACE pour tirer"
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + h
  love.graphics.print(content, x , y) 

  content = "Appuyer sur ESC pour quitter"
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + h
  love.graphics.print(content, x , y) 

  love.graphics.setColor(255, 255, 255, 255)
end --drawStartScreen

function drawEndScreen ()
  if (playerHasWon) then 
    love.graphics.draw (endScreenImageWin)
  else 
    love.graphics.draw (endScreenImageLose)
  end
    
  local w, h, font, x, y
  font = fontMenu
  
  love.graphics.setFont(font)
  love.graphics.setColor(255, 255, 255, 255)
  h = font:getHeight(content)
  y = (screenHeight - h) / 2
  
  content = "Votre Score est "..playerScore
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + 4 * h
  love.graphics.print(content, x , y) 
  
  content = "Appuyer sur R pour rejouer"
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + 2 * h
  love.graphics.print(content, x , y) 

  content = "Appuyer sur ESC pour quitter"
  w = font:getWidth(content)
  x = (screenWidth - w) / 2
  y = y + h
  love.graphics.print(content, x , y) 
  
  love.graphics.setColor(255, 255, 255, 255)  
end --drawEndScreen

function keypressedPlayScreen (key)
  --print(key)
  if (key == "space") then playerFire() end
end --keypressedPlayScreen

function keypressedStartScreen (key)
  if (key == "space") then startGame(1, true) end
end --keypressedStartScreen

function keypressedEndScreen (key)
  if (key == "r") then startGame(1, true) end
end --keypressedEndScreen

function spawnToMap (pSprite, l, c)
  pSprite.x, pSprite.y = pixelToMap (l, c)
end

function pixelToMap(c, l)
  local x = map.tilewidth * c - map.tilewidth / 2
  local y = map.tileheight * (l - 1) -  map.tileheight / 2 
  return x, y
end --pixelToMap

function createSprite (pNomImage, pX, pY, pvX, pvY, pType, pScale, pLife, pPoints)
  if (pType == nil) then pType = SPRITE_TYPE_UNDEFINED end
  if (pX == nil) then pX = 0 end
  if (pY == nil) then pY = 0 end
  if (pvX == nil) then pvX = 0 end
  if (pvY == nil) then pvY = 0 end
  if (pScale == nil) then pScale = spriteScale end
  if (pLife == nil) then pLife = 1 end
  if (pPoints == nil) then pPoints = (pLife * 10 + 5) end

  local sprite = {}
  local w, h
  sprite.type= pType
  sprite.status = SPRITE_STATE_NORMAL
  sprite.image = love.graphics.newImage (FOLDER_IMAGES.."/"..pNomImage..".png")
  w, h = sprite.image:getDimensions()
  sprite.w = w * pScale
  sprite.h = h * pScale
  sprite.x = pX
  sprite.y = pY
  sprite.vX = pvX
  sprite.vY = pvY
  sprite.scale = pScale
  sprite.life = pLife
  sprite.frame = 1
  sprite.animationFrames = {}
  sprite.maxFrame = 1 -- note: si cette valeur est > 1, le sprite est animé
  sprite.destroyDelay = 0 -- délai avant la destruction effective du sprite. Permet de laisser du temps entre l'affichage de l'explosion et la suppression (utile pour les boss et le joueur)
  sprite.fireAngle = 0 
  sprite.destroyAtBottom = true
  sprite.fireRate = {
    min = 1,
    max = 2
  } 
  sprite.points = pPoints
  
  table.insert(listSprites, sprite)
  return sprite
end --createSprite

function createFX (pType, pX, pY, pScale)
  if (pType == nil) then pType = SPRITE_TYPE_VFX__EXPLODE end
  if (pX == nil) then pX = 0 end
  if (pY == nil) then pY = 0 end
  if (pScale == nil) then pScale = spriteScale end
  
  local index, w, h
  w = player.w / 2
  h = player.h / 2

  -- explosion "normale) (projectile)
  if (pType == SPRITE_TYPE_VFX__EXPLODE) then
    sprite = createSprite("explode_1", pX, pY, 0, 0, SPRITE_TYPE_VFX__EXPLODE, pScale)
    sprite.animationFrames = imgExplosionPlayer
    sprite.maxFrame = #sprite.animationFrames
  -- explosion du joueur
  elseif (pType == SPRITE_TYPE_VFX__EXPLODE_PLAYER) then
    -- on crée une grosse explosion (entre 3 et 6 "petites" explosions)
    for index = 1, math.random(3, 6) do
      createFX(SPRITE_TYPE_VFX__EXPLODE, pX + math.random(-w, w) , pY + math.random(-h, h))
    end
  -- explosion d'un PNJ
  elseif (pType == SPRITE_TYPE_VFX__EXPLODE_PNJ) then
    -- on crée une grosse explosion (entre 2 et 4 "petites" explosions)
    for index = 1, math.random(2, 4) do
      createFX(SPRITE_TYPE_VFX__EXPLODE, pX + math.random(-w, w) , pY + math.random(-h, h))
    end
   -- explosion d'un PNJ Boss
  elseif (pType == SPRITE_TYPE_VFX__EXPLODE_BOSS) then
    -- on crée une plus grosse explosion (entre 10 et 20 "petites" explosions)
    for index = 1, math.random(10, 20) do
      createFX(SPRITE_TYPE_VFX__EXPLODE, pX + math.random(-w, w) , pY + math.random(-h, h))
    end   
    -- and so on ...
  end
  return sprite
end --createFX

function createPlayer (pX, pY, pvX, pvY, pScale, pLife)
  if (pX == nil) then pX = 0 end
  if (pY == nil) then pY = 0 end
  if (pvX == nil) then pvX = 0 end
  if (pvY == nil) then pvY = 0 end
  if (pScale == nil) then pScale = spriteScale end
  if (pLife == nil) then pLife = 1 end

  local sprite = createSprite ("player", pX, pY, pvX, pvY, SPRITE_TYPE_PLAYER, pScale, pLife)
  sprite.destroyDelay = 3 --le player reste à l'écran pendant quelques seconde avant la fin du jeu
  
  return sprite
end --createPlayer

function createProjectile(pType, pX, pY, pScale, pLife)
  if (pType == nil) then pType = SPRITE_TYPE_PROJ_PLAYER__1 end
  if (pX == nil) then pX = 0 end
  if (pY == nil) then pY = 0 end
  if (pScale == nil) then pScale = spriteScale end
  if (pLife == nil) then pLife = 1 end
  
  local dirX = 0.7
  local dirY = 0.7
  
  -- on accélère les vitesses de déplacement en fonction du niveau du joueur
  dirX = dirX * (1 + playerLevel / levelToWin)
  dirY = dirY * (1 + playerLevel / levelToWin)
  
  local sprite = {}
  -- projectiles joueur (vers le haut)
  if (pType == SPRITE_TYPE_PROJ_PLAYER__1) then
    sprite = createSprite ("projectile_player_1", pX, pY, 0, -6 * dirY, pType, pScale, pLife)
  elseif (pType == SPRITE_TYPE_PROJ_PLAYER__2) then
    sprite = createSprite ("projectile_player_2", pX, pY, 0, -10 * dirY, pType, pScale, pLife)
  elseif (pType == SPRITE_TYPE_PROJ_PLAYER__3) then
    sprite = createSprite ("projectile_player_3", pX, pY, 0, -15 * dirY, pType, pScale, pLife)
  -- projectiles PNJ (vers le bas)
  elseif (pType == SPRITE_TYPE_PROJ_PNJ__1) then
    sprite = createSprite ("projectile_pnj_1", pX, pY, 0, 4 * dirY, pType, pScale, pLife)
  elseif (pType == SPRITE_TYPE_PROJ_PNJ__2) then
    sprite = createSprite ("projectile_pnj_2", pX, pY, 0, 8 * dirY, pType, pScale, pLife)
  elseif (pType == SPRITE_TYPE_PROJ_PNJ__3) then
    sprite = createSprite ("projectile_pnj_3", pX, pY, 0, 10 * dirY, pType, pScale, pLife)

    -- and so on ...
  end

  table.insert(listProjectiles, sprite)
  return sprite
end --createProjectile

function createPNJ(pType, pX, pY, pScale)
  if (pType == nil) then pType = SPRITE_TYPE_PNJ__1 end
  if (pX == nil) then pX = 0 end
  if (pY == nil) then pY = 0 end
  if (pScale == nil) then pScale = spriteScale end

  local sprite = {}
  
  local speedMultiplierX = 0.7
  local speedMultiplierY = 0.7
  
  -- on accélère les vitesses de déplacement en fonction du niveau du joueur
  speedMultiplierX = speedMultiplierX * (1 + playerLevel / levelToWin)
  speedMultiplierY = speedMultiplierY * (1 + playerLevel / levelToWin)
  
  -- Change aléatoirement le sens de déplacement en X
  if (math.random(1, 2) == 1) then speedMultiplierX = -speedMultiplierX end
  -- Change aléatoirement le sens de déplacement en Y
  if (math.random(1, 2) == 1) then speedMultiplierY = -speedMultiplierY end

  if (pType == SPRITE_TYPE_PNJ__1) then
    sprite = createSprite ("pnj_1", pX, pY, 2 * speedMultiplierX, 0, pType, pScale, 1) -- vie a 1
    sprite.fireRate = {
      min = 5 * coeffForFireRateWithDt, 
      max = 10 * coeffForFireRateWithDt 
    }
  elseif (pType == SPRITE_TYPE_PNJ__2) then
    sprite = createSprite ("pnj_2", pX, pY, 5 * speedMultiplierX, 0, pType, pScale, 2) -- vie a 2
    sprite.fireRate = {
      min = 4 * coeffForFireRateWithDt,
      max = 6 * coeffForFireRateWithDt
    }
  elseif (pType == SPRITE_TYPE_PNJ__3) then
    sprite = createSprite ("pnj_3", pX, pY, 5 * speedMultiplierX, 2 * speedMultiplierY, pType, pScale, 3) -- vie a 6
    sprite.fireRate = {
      min = 2 * coeffForFireRateWithDt,
      max = 3 * coeffForFireRateWithDt
    }
  elseif (pType == SPRITE_TYPE_PNJ__4) then --tourelle
    sprite = createSprite ("pnj_4", pX, pY, 0, 0, pType, pScale, 5) -- vie a 5, pas de déplacement
    sprite.fireRate = {
      min = 4 * coeffForFireRateWithDt,
      max = 6 * coeffForFireRateWithDt
    }
  elseif (pType == SPRITE_TYPE_PNJ__BOSS__1) then --boss
    sprite = createSprite ("pnjBoss_1", pX, pY, 1 * speedMultiplierX, 2 * speedMultiplierY, pType, pScale , 50) -- vie a 50, 2 * plus gros que le 3, reste stationnaire
    sprite.fireRate = {
      min = 0.5 * coeffForFireRateWithDt,
      max = 1 * coeffForFireRateWithDt
    }  
    sprite.destroyAtBottom = false
    sprite.destroyDelay = 6
    -- and so on ...
  end

  sprite.status = SPRITE_STATE_SLEEPING
  sprite.fireTimer = sprite.fireRate.max or 1 * coeffForFireRateWithDt 

  table.insert(listPNJ, sprite)
  return sprite
end

function drawSprite (pSprite)
  if (pSprite.image ~= nil and pSprite.image ~="") then 
    love.graphics.draw(pSprite.image, pSprite.x, pSprite.y, 0, pSprite.scale, pSprite.scale, pSprite.w / (2 * pSprite.scale), pSprite.h / (2 * pSprite.scale))
  end
end --drawSprite

function getScreenLimits (pSprite)
  local xMin = pSprite.w / 2
  local yMin = pSprite.h / 2
  local xMax = screenWidth - pSprite.w / 2
  local yMax = screenHeight - pSprite.h / 2
  return xMin, yMin, xMax, yMax
end --getScreenLimits


function HUDUpdate (pDt)
  if (screenMessage.timer < screenMessage.delai) then screenMessage.timer = screenMessage.timer + pDt end
end

function HUDDraw ()
  -- Affichage du message "automatique" (avec timer)
  ----------------------
  -- si un texte a été défini pour le message , on l'affiche pendant le délai défini
  if (screenMessage ~= nil and screenMessage.text ~= nil and screenMessage.text ~= "") then
    if (screenMessage.timer < screenMessage.delai) then
      -- affiche le message à l'écran
      love.graphics.setFont(screenMessage.font)
      love.graphics.setColor(screenMessage.color)
      local w = screenMessage.font:getWidth( screenMessage.text)
      local h = screenMessage.font:getHeight( screenMessage.text)
      love.graphics.print( screenMessage.text, (screenWidth - w) / 2 , (screenHeight - h) / 2) -- texte centré
      love.graphics.setColor(255, 255, 255, 255)
    else
      -- réinitialise le délai et efface le message
      screenMessage.timer = 0
      screenMessage.text = ""
    end
  end
  
end --messageDraw

function mapLoad (pLevel)
  if (pLevel == nil) then pLevel = 1 end
  local index
  local filename = FOLDER_MAP_LEVELS.."/level"..pLevel

  if (love.filesystem.exists(filename..".lua")) then
    map = require(filename)
  else
    print("map.load:le fichier "..filename.." n'a pas été trouvé")
    -- plus de niveaux à charger, la partie est gagnée
    winGame()
  end --if (love.filesystem.exists(filename)) then
  return map
end --mapLoad

function mapDraw ()
  local l, c, indexTile, x, y, indexGrid, grid, tileSheet, indexLayer, indexTileSheet, quad, w, h
  indexLayer = 1
  indexTileSheet = 1
  w = map.tilewidth
  h = map.tileheight
  grid = map.layers[indexLayer].data
  tileSheet = love.graphics.newImage (FOLDER_MAP_IMAGES.."/"..map.tilesets[indexTileSheet].image)
  
  -- pour le scrolling, on parcours la map à l'envers
  x = 0
  y = -h + camera.y
   
  indexGrid = 1
  for l =  map.height, 1, -1 do
    for c = 1, map.width do
      indexGrid = (l - 1) * map.width + c
      indexTile = grid[indexGrid]
      if (indexTile ~= nil and tonumber(indexTile) > 0) then
        quad = love.graphics.newQuad((indexTile -1) * w, 0, w, h, tileSheet:getDimensions())
        love.graphics.draw(tileSheet, quad, x , y, 0, spriteScale, spriteScale)
      end
      x = x + w * spriteScale
    end
    x = 0
    y = y - h * spriteScale
  end
end --mapDraw

-- initialise la camera
function cameraInit (pvX, pvY)
  if (pvX == nil) then pvX = 0 end
  if (pvY == nil) then pvY = 0 end
  camera.x = 0
  camera.y = 0
  camera.vX = pvX
  camera.vY = pvY
end --cameraInit

function playerFire()
  local projectile = createProjectile (SPRITE_TYPE_PROJ_PLAYER__1, 0, 0, spriteScale)
  -- ajuste la position de départ du projectile en fonction de ses dimensions
  projectile.x = player.x 
  projectile.y = player.y - player.h / 2
  sndPlayerFire:play()
end --playerFire

function pnjFire(pSprite)
  local projectile, vX, vY, speed

  -- le projectile et la façon de le tirer (vitesses) dépend du type de sprite
  if (pSprite.type == SPRITE_TYPE_PNJ__1) then --vaisseau 1
    projectile = createProjectile (SPRITE_TYPE_PROJ_PNJ__1, 0, 0, spriteScale) -- on garde les valeurs de vitesses associées par défaut au type de projectile
  elseif (pSprite.type == SPRITE_TYPE_PNJ__2) then --vaisseau 2
    projectile = createProjectile (SPRITE_TYPE_PROJ_PNJ__2, 0, 0, spriteScale) -- on garde les valeurs de vitesses associées par défaut au type de projectile
  elseif (pSprite.type == SPRITE_TYPE_PNJ__3) then --vaisseau 3
    projectile = createProjectile (SPRITE_TYPE_PROJ_PNJ__3, 0, 0, spriteScale) -- on garde les valeurs de vitesses associées par défaut au type de projectile
    -- le projectile est dirigé vers le player
    if (projectile.vY > projectile.vX) then speed = projectile.vY else speed = projectile.vX end
    pSprite.fireAngle = math.angle(pSprite.x, pSprite.y, player.x, player.y)
    vX = speed * math.cos(pSprite.fireAngle)
    vY = speed * math.sin(pSprite.fireAngle)
    projectile.vX = vX
    projectile.vY = vY
  elseif (pSprite.type == SPRITE_TYPE_PNJ__4) then -- tourelle
    projectile = createProjectile (SPRITE_TYPE_PROJ_PNJ__3, 0, 0, spriteScale) -- on garde les valeurs de vitesses associées par défaut au type de projectile
    -- le projectile est dirigé vers le player
    if (projectile.vY > projectile.vX) then speed = projectile.vY else speed = projectile.vX end
    pSprite.fireAngle = math.angle(pSprite.x, pSprite.y, player.x, player.y)
    vX = speed * math.cos(pSprite.fireAngle)
    vY = speed * math.sin(pSprite.fireAngle)
    projectile.vX = vX
    projectile.vY = vY
    -- and so on ...
  elseif (pSprite.type == SPRITE_TYPE_PNJ__BOSS__1) then -- boss
    projectile = createProjectile (SPRITE_TYPE_PROJ_PNJ__3, 0, 0, spriteScale) -- on garde les valeurs de vitesses associées par défaut au type de projectile
    -- le projectile est en rotation vers le bas du boss
    if (projectile.vY > projectile.vX) then speed = projectile.vY else speed = projectile.vX end
    local angleIncrement = 0.2 + playerLevel / 50
    -- 1 chance sur 10 de changer le sens du tir 
    if (math.random(1, 10) > 9) then angleIncrement = -angleIncrement end
    pSprite.fireAngle = pSprite.fireAngle + angleIncrement
    
    vX = speed * math.cos(pSprite.fireAngle)
    vY = speed * math.sin(pSprite.fireAngle)
    projectile.vX = vX
    projectile.vY = vY
    
    -- and so on ...
  end

  if (projectile ~= nil) then
    -- point de départ du projectile (depend de ses dimension)
    projectile.x = pSprite.x 
    projectile.y = pSprite.y + (pSprite.h - projectile.h) / 2
    sndpnjFire:play()
  else
    print("pnjFire:problème lors de la création du projectile")
  end
end --pnjFire

function spriteBoxCollide (pSprite1, pSprite2)
  if (
    (pSprite1 == pSprite2) 
    or(pSprite1.status == SPRITE_STATE_DESTROYED or pSprite1.status == SPRITE_STATE_DESTROYING)
    or(pSprite2.status == SPRITE_STATE_DESTROYED or pSprite2.status == SPRITE_STATE_DESTROYING)
  )
  then 
    return false
  end
  local dx = pSprite1.x - pSprite2.x
  local dy = pSprite1.y - pSprite2.y
  --if (math.abs(dx) < pSprite1.w + pSprite2.w) and (math.abs(dy) < pSprite1.h + pSprite2.h) then return true end
  if (math.abs(dx) < pSprite1.image:getWidth() + pSprite2.image:getWidth()) then
  if (math.abs(dy) < pSprite1.image:getHeight() + pSprite2.image:getHeight()) then
   return true
 end
 
  end
  return false
end --spriteBoxCollide

function projectileDestroy (pProjectile, pListIndex, pShowFX)
  if (pProjectile.status == SPRITE_STATE_DESTROYING or pProjectile.status == SPRITE_STATE_DESTROYED) then return end
  
  if (pShowFX == nil) then pShowFX = true end
  
  if (pShowFX) then createFX(SPRITE_TYPE_VFX__EXPLODE, pProjectile.x, pProjectile.y) end
  
  pProjectile.status = SPRITE_STATE_DESTROYED -- nécessaire pour l'effacer de la liste des sprites
  table.remove(listProjectiles, pListIndex)
end --projectileDestroy

function pnjDestroy(pPNJ, pListIndex, mustAddScore)
  if (mustAddScore == nil) then mustAddScore = true end
  
  if (pPNJ.status == SPRITE_STATE_DESTROYING or pPNJ.status == SPRITE_STATE_DESTROYED) then return end

  if (pPNJ.type:find(SPRITE_TYPE_PNJ__BOSS) ~= nil) then
    -- un pnj Boss
    createFX(SPRITE_TYPE_VFX__EXPLODE_BOSS, pPNJ.x, pPNJ.y)
  else
    -- pnj normal
    createFX(SPRITE_TYPE_VFX__EXPLODE_PNJ, pPNJ.x, pPNJ.y)
  end
  
  if (mustAddScore and pPNJ.type:find(SPRITE_TYPE_PNJ) ~= nil) then 
    playerScore = playerScore + pPNJ.points
  end
  
  sndpnjDestroy:play()
  pPNJ.image = "" --permet de ne plus afficher le player pendant le délai de sa destruction effective
  pPNJ.status = SPRITE_STATE_DESTROYING -- note: la destruction réelle du sprite est déclenchée dans screenPlayUpdate quand le destroyDelay est écoulé
end --pnjDestroy

function playerDestroy ()
  if (player.status == SPRITE_STATE_DESTROYING or player.status == SPRITE_STATE_DESTROYED) then return end
  
  createFX(SPRITE_TYPE_VFX__EXPLODE_PLAYER, player.x, player.y)
  sndPlayerDestroy:play()
  player.image = "" --permet de ne plus afficher le player pendant le délai de sa destruction effetive
  player.status = SPRITE_STATE_DESTROYING -- note: la fin de partie est déclenchée dans screenPlayUpdate quand le destroyDelay est écoulé
end --playerDestroy

function projectileLoseLife (pProjectile, pListIndex)
  --print ("le projectile"..pListIndex.." perd une vie")
  pProjectile.life = pProjectile.life - 1
  if (pProjectile.life <= 0) then 
    projectileDestroy(pProjectile, pListIndex)
  end
end --projectileLoseLife

function pnjLoseLife (pPNJ, pListIndex)
  --print ("le PNJ "..pListIndex.." perd une vie")
  sndpnjLoseLife:play()
  pPNJ.life = pPNJ.life - 1
  if (pPNJ.life <= 0) then 
    pnjDestroy(pPNJ, pListIndex)
  end
end --pnjLoseLife

function playerLoseLife ()
  --print ("le joueur perd une vie")
  sndPlayerLoseLife:play()
  player.life = player.life - 1
  if (player.life <= 0) then
    playerDestroy()
  end
end --playerLoseLife

function startGame (pLevel, mustResetPlayer)
  if (pLevel == nil) then pLevel = playerLevel end
  if (mustResetPlayer == nil) then mustResetPlayer = false end
  map = nil
  map = mapLoad(pLevel) 
  
  if (map ~= nil) then 
  
    -- on réinitialise les objets, sauf le joueur
    listProjectiles = {}
    listPNJ = {}
    listSprites = {}
    if (mustResetPlayer) then 
      player = createPlayer (0, 0, 7, 7, spriteScale, 3) -- vie à 3
      playerScore = 0
      playerLevel = 1
      playerHasWon = false    
    else 
      table.insert(listSprites, player)
    end   
    
    -- init de la camera, qui en se déplaçant automatiquement va permettre un scrolling
    cameraInit(0, 1) -- avance de 1 pixel en Y  

    -- place le joueur en bas de l'écran
    local xMin, yMin, xMax, yMax = getScreenLimits(player)
    player.x = (xMax - player.w) / 2
    player.y = yMax - player.h / 2

    spawnPnjs(playerLevel)
    
    screen = SCREEN_PLAY
    
    screenMessage.color = {255, 128, 0, 255}
    screenMessage.text = "Niveau "..pLevel
    screenMessage.timer = 0
    
    gameMustBeReStarted = true
  end
end --startGame

function winGame ()
  print ("winGame")
  playerHasWon = true
  screen = SCREEN_END
  screenMessage.text = "Game OVER"
  screenMessage.color = {200, 0, 0, 255}
end --winGame

function loseGame ()
  print ("loseGame")
  playerHasWon = false
  screen = SCREEN_END
  screenMessage.text = "Game OVER"
  screenMessage.color = {200, 0, 0, 255}
end --loseGame

function nextLevel ()
  print ("NIVEAU SUIVANT")
  screenMessage.text = "Boss Killer !"
  screenMessage.color = {200, 200, 0, 255}
  playerLevel = playerLevel + 1
  if (playerLevel < levelToWin) then
    startGame(playerLevel, false)
  else
    winGame ()
  end
end --nextLevel

function spawnPnjs(pLevel)
  if (pLevel == nil) then pLevel = 1 end
  local enemy
  local cMin, cMax, lMin, lMax, index
  cMin = 2
  lMin = 1
  cMax = map.width -1
  lMax = map.height - 5
  nbTypePnj = 4
  pnjCount = {}
  
  -- nombre de pnj de chaque catégorie
  for index = 1, nbTypePnj do
      local tmpVal = nbTypePnj - index + 1
      pnjCount[index] = pLevel * tmpVal + tmpVal * 2
      --print ("nb de type "..index, pnjCount[index] )
  end 

  -- on génère les pnj en fonction du niveau du joueur
  -- TODO: repartir plus de pnj plus coriaces en fin de niveau et les plus faible au début
  for index = 1, pnjCount[1] do
    enemy = createPNJ(SPRITE_TYPE_PNJ__1, 0, 0, spriteScale)
    cRand = math.random(cMin, cMax)
    lRand = math.random(lMin, lMax)
    spawnToMap(enemy, cRand, -lRand)
  end

  for index = 1, pnjCount[2] do
    enemy = createPNJ(SPRITE_TYPE_PNJ__2, 0, 0, spriteScale)
    cRand = math.random(cMin, cMax)
    lRand = math.random(lMin, lMax)
    spawnToMap(enemy, cRand, -lRand)
  end
  
  for index = 1, pnjCount[3] do
    enemy = createPNJ(SPRITE_TYPE_PNJ__3, 0, 0, spriteScale)
    cRand = math.random(cMin, cMax)
    lRand = math.random(lMin, lMax)
    spawnToMap(enemy, cRand, -lRand)
  end

  for index = 1, pnjCount[4] do
    enemy = createPNJ(SPRITE_TYPE_PNJ__4, 0, 0, spriteScale)
    cRand = math.random(cMin, cMax)
    lRand = math.random(lMin, lMax)
    spawnToMap(enemy, cRand, -lRand)
  end
  
  -- le BOSS
  enemy = createPNJ(SPRITE_TYPE_PNJ__BOSS__1, 0, 0, spriteScale)
  spawnToMap(enemy, 10, -map.height) -- fin du niveau
  --spawnToMap(enemy, 2, 1) --pour test
end --spawnPnjs