### Description
Un prototype de jeu ShootThemUp avec un scrolling vertical et une vue de dessus, écrit en LUA et Love2D.  

===
### Objectifs
Détruire tous les ennemis et survivre.  
La partie est gagnée lorsque le niveau 3.  

### Mouvements et actions du joueur
Déplacer le joueur: les flèches du clavier.   
Tirer: touche espace.    
Relancer la partie: touche R.  
Quitter le jeu: clic sur la croix ou touche escape.  

### Interactions
Le joueur peut tirer sur les ennemis.  
Les ennemis peuvent tirer sur le joueur.  

### Copyrights
Ecrit en LUA et en utilisant le Framework Love2D.  
Développé sans outil spécifique, en utilisant principalement SublimeText 3 et ZeroBrane Studio (pour le débuggage).  

Inspiré du cours dispensé par l'école de formation en ligne "GameCodeur", disponible sur le site http://www.gamecodeur.fr

-----------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)

=================================================

### Description
A ShootThemUp game prototype with a vertical scrolling and a top view, written in LUA and Love2D.  

===
### Goals
Destroy all enemies and survive.  
The game is won when level 3 is reached.  

### Movements and actions of the player
Move the player: the arrows on the keyboard.
Fire: space key.
Restart the game: R key.  
Exit the game: click on the cross or escape key.  

### Interactions
The player can shoot the enemies.  
Enemies can shoot the player.  

### Copyrights
Written in LUA and with the Love2D Framework.  
Developed without any specific tool, mainly using SublimeText 3 and ZeroBrane Studio (for debugging).  

Inspired by the course given by the online training school "GameCodeur", available on the site http://www.gamecodeur.fr

------------------
(C) 2017 GameCoderBlog (http://www.gamecoderblog.com)